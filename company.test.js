const comp = require('./company');

test('entering a string as an id in company gives an error', () => {
    expect(() => {
       comp('ss');
    }).toThrow('id needs to be integer');

});