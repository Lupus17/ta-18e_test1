const toNumber = require('./toNumber');

describe('toNumber', () => {
    test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
    });

    test('"1" => 1', () => {
        expect(toNumber('1')).toBe(1);
    });

    test('"a" is error', () => {
        expect(() => {
            toNumber('a');
        }).toThrow('value \'a\' is not a number!');
    });

    test('"-" is error', () => {
        expect(() => {
            toNumber('-');
        }).toThrow('value \'-\' is not a number!');
    });
    test('"0.5" is converted correctly', () => {
        expect(() => {
            toNumber(toNumber('0.5').toBe(0.5));
        });
    });
});
